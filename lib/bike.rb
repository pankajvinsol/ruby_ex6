require_relative "vehicle.rb"

class Bike < Vehicle

  attr_accessor :dealer

  def initialize(bike_name, bike_price, bike_dealer)
    super(bike_name, bike_price)
    self.dealer = bike_dealer
  end
  
  def to_s
    super + ", Dealer: #{ dealer }"
  end

end

