class Vehicle

  attr_accessor :price, :name

  def initialize(vehicle_name, vehicle_price)
    self.name = vehicle_name
    self.price = vehicle_price
  end

  def to_s
    "Vehicle Name: #{ name }, Price: #{ price }"
  end

end

